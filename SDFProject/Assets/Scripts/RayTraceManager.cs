﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayTraceManager : MonoBehaviour
{

    public ComputeShader rayTraceShader;
    public Texture SkyboxTexture;
    public Light light;

    private RenderTexture rtTarget = null;
    private Camera camera = null;

    // Start is called before the first frame update
    void Start()
    {
        camera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDestroy()
    {
        rtTarget.Release();
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        RunComputeRenderer(destination);
    }
    private void SetShaderParameters()
    {
        rayTraceShader.SetMatrix("_CameraToWorld", camera.cameraToWorldMatrix);
        rayTraceShader.SetMatrix("_CameraInverseProjection", camera.projectionMatrix.inverse);
        rayTraceShader.SetTexture(0, "_SkyboxTexture", SkyboxTexture);

        Vector3 l = light.transform.forward;
        rayTraceShader.SetVector("_DirectionalLight", new Vector4(l.x, l.y, l.z, light.intensity));

        rayTraceShader.SetTexture(0, "Result", rtTarget);
    }

    private void RunComputeRenderer(RenderTexture destination)
    {
        //run Init, in case we need to create/redo texture
        InitRenderTexture();
        SetShaderParameters();

        //Thread group info. (it works based on two layers of thread groups, the first defined here, the second defined in the shader.
        //The one defined in the shader is the value we divide the screen by, so that we can work on various screen resolutions.
        //To support maximum resolutions, should use a value that a majority of resolutions run at. Currently 8, which should work for most modern resolutions. 
        //To support more, 2 or 4 should be used.
        //The dispatch function also has a Z component to run thread groups in, this is not needed at this time.
        int threadGroupsX = Mathf.CeilToInt(Screen.width / 8.0f);
        int threadGroupsY = Mathf.CeilToInt(Screen.height / 8.0f);
        //This is blocking.
        rayTraceShader.Dispatch(0, threadGroupsX, threadGroupsY, 1);

        //Possible optimization, find a way to not use blit...
        Graphics.Blit(rtTarget, destination);
    }

    void InitRenderTexture()
    {
        if(rtTarget == null || (rtTarget.width != Screen.width || rtTarget.height != Screen.height))
        {
            //either the texture isn't ready, or needs to be remade.
            //If we have an existing texture, free it.
            if(rtTarget != null)
            {
                rtTarget.Release();
            }

            //Create texture.
            rtTarget = new RenderTexture(Screen.width, Screen.height,0,RenderTextureFormat.ARGBFloat, RenderTextureReadWrite.Linear);
            rtTarget.enableRandomWrite = true;
            rtTarget.Create();
        }
    }
}
